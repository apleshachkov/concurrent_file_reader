package com.example.concurrent_file_reader;

import com.example.concurrent_file_reader.service.FileReadingService;
import com.example.concurrent_file_reader.service.FileTransferService;
import com.example.concurrent_file_reader.service.impl.FileReadingServiceImpl;
import com.example.concurrent_file_reader.service.impl.FileTransferServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.Collection;

@SpringBootApplication
public class ConcurrentFileReaderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConcurrentFileReaderApplication.class, args);
        String mask = args[0];
        String path = args[1];

        System.out.println("File mask: " + mask + ", path: " + path);
        FileReadingService readingService = new FileReadingServiceImpl();
        FileTransferService transferService = new FileTransferServiceImpl();
        Collection<File> found = readingService.findAndRead(path, mask);
        transferService.transfer(found);
    }
}
