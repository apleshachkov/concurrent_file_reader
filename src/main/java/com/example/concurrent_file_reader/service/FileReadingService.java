package com.example.concurrent_file_reader.service;

import java.io.File;
import java.util.Collection;

public interface FileReadingService {

    Collection<File> findAndRead(String dir, String mask);
}
