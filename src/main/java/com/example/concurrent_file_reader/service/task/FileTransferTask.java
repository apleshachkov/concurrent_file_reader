package com.example.concurrent_file_reader.service.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.concurrent.Callable;



public class FileTransferTask implements Callable<String> {

    private static final Logger LOG = LoggerFactory.getLogger(FileTransferTask.class);
    private final WebClient localApiClient;
    private final File file;
    private final String uri;

    public FileTransferTask(WebClient localApiClient, File file, String uri) {
        this.localApiClient = localApiClient;
        this.file = file;
        this.uri = uri;
    }

    @Override
    public String call() {
        try {
            return transferFile(file);
        } catch (Exception e) {
            LOG.debug("Something went wrong with transfer file {}: {}", file.getName(), e.getCause());
            return null;
        }
    }

    private String transferFile(File file) throws URISyntaxException, IOException {
        return localApiClient.post()
                .uri(new URI(uri))
                .bodyValue(Files.readAllBytes(file.toPath()))
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}
