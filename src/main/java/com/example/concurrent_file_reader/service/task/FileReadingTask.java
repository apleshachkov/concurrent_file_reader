package com.example.concurrent_file_reader.service.task;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileReadingTask implements Runnable {
    private final int fileIndex;
    private final String mask;
    private final List<File> fileList;
    private final Map<String, File> filePathList = new HashMap<>();
    private final TaskFinishListener listener;

    public FileReadingTask(int fileIndex, String mask, List<File> fileList, TaskFinishListener listener) {
        this.fileIndex = fileIndex;
        this.mask = mask;
        this.fileList = fileList;
        this.listener = listener;
    }

    @Override
    public void run() {
        File currentFile = fileList.get(fileIndex);
        if (currentFile != null && currentFile.getName().contains(mask)) {
            filePathList.put(currentFile.getAbsolutePath(), currentFile);
        }

        listener.onFinish(filePathList);
    }
}
