package com.example.concurrent_file_reader.service.task;

import java.io.File;
import java.util.Map;

@FunctionalInterface
public interface TaskFinishListener {
    void onFinish(Map<String, File> filePathList);
}
