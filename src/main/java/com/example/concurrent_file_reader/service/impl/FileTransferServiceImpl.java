package com.example.concurrent_file_reader.service.impl;

import com.example.concurrent_file_reader.config.ApplicationContextProvider;
import com.example.concurrent_file_reader.service.FileTransferService;
import com.example.concurrent_file_reader.service.task.FileTransferTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
public class FileTransferServiceImpl implements FileTransferService {

    private static final String URI = "http://localhost:8081/accept-files";
    private static final Logger LOG = LoggerFactory.getLogger(FileTransferServiceImpl.class);

    @Override
    public void transfer(Collection<File> files) {
        if (files == null || files.isEmpty()) {
            return;
        }

        files.stream()
                .filter(File::exists)
                .filter(File::isFile)
                .forEach(file -> {
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<String> status = executor.submit(
                    new FileTransferTask(
                            (WebClient) ApplicationContextProvider.getApplicationContext().getBean("localApiClient"),
                            file, URI));
            try {
                System.out.println("File transfer is done: " + status.get());
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
                LOG.debug("Something went wrong with transfer file {}: {}", file.getName(), e.getCause());
            }
        });
    }
}
