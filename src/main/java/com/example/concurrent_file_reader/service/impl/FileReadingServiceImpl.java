package com.example.concurrent_file_reader.service.impl;

import com.example.concurrent_file_reader.service.FileReadingService;
import com.example.concurrent_file_reader.service.task.FileReadingTask;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class FileReadingServiceImpl implements FileReadingService {

    private final AtomicInteger fileIndex;
    private int taskDoneCount;

    public FileReadingServiceImpl() {
        this.fileIndex = new AtomicInteger(-1);
        this.taskDoneCount = 0;
    }

    @Override
    public Collection<File> findAndRead(String dir, String mask) {
        if (dir == null || dir.isEmpty()) {
            throw new RuntimeException("Path cannot be null or empty");
        }
        File dirrr = new File(dir);

        if (!dirrr.exists() || !dirrr.isDirectory()) {
            throw new RuntimeException("Wrong path, try again");
        }

        if (dirrr.listFiles() == null || Objects.requireNonNull(dirrr.listFiles()).length == 0) {
            throw new RuntimeException("No any files in directory");
        }

        List<File> fileList = new ArrayList<>(Arrays.asList(Objects.requireNonNull(dirrr.listFiles())));

        subdirectoryFill(fileList);
        return fillResultPathList(fileList, mask);
    }

    private Collection<File> fillResultPathList(List<File> fileList, String mask) {
        Map<String, File> resultMap = new ConcurrentHashMap<>();
        ExecutorService executor = Executors.newFixedThreadPool(fileList.size());
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < fileList.size(); i++) {
            int step = fileIndex.incrementAndGet();
            executor.submit(new FileReadingTask(step, mask, fileList, filePathList -> {
                System.out.printf("Thread with index %s started %n", step);
                synchronized (this) {
                    taskDoneCount++;
                    resultMap.putAll(filePathList);
                    if (taskDoneCount == fileList.size()) {
                        executor.shutdown();
                        System.out.println("All found files: " + resultMap.size() + "\n");
                        resultMap.keySet().forEach(System.out::println);
                        System.out.println("Time used: " + (System.currentTimeMillis() - startTime) + "ms");
                    }
                }
            }));
        }

        return resultMap.values();
    }

    private void subdirectoryFill(List<File> fileList) {
        List<File> subdirectoryFiles = new ArrayList<>();
        for (File file : fileList) {
            if (file != null && file.isDirectory() && file.listFiles() != null) {
                subdirectoryFiles.addAll(Arrays.asList(Objects.requireNonNull(file.listFiles())));
            }
        }
        fileList.addAll(subdirectoryFiles);
    }

    private void resultCompare() {

    }
}
