package com.example.concurrent_file_reader.service;

import java.io.File;
import java.util.Collection;

public interface FileTransferService {

    void transfer(Collection<File> files);
}
