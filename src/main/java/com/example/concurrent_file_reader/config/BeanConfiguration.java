package com.example.concurrent_file_reader.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class BeanConfiguration {

    @Bean
    public WebClient localApiClient() {
        return WebClient.create("http://localhost:8080/api/v3");
    }
}
