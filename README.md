# concurrent_file_reader
JDK 17, Spring Boot.
Порт по дефолту - 8080.
Отвечает за поиск файлов по маске в указанной директории и поддиректориях. 
Выводит ко-во найденных файлов и их перечень на консоль.
При наличии найденных файлов - по одному пересылает на API приложения-ресивера.

Содержит набор тестовых файлов в директории test_data.

# Параметры локального запуска:
- Spring Boot Application -> run с двумя аргументами: маска файла и путь к директории для поиска.
- cmd -> java -jar path_to_source\concurrent_file_reader-0.0.1-SNAPSHOT.jar *маска* *путь*
Пример: java -jar C:\work\...\concurrent_file_reader-0.0.1-SNAPSHOT.jar test_file C:\\work\\projects\\concurrent_file_reader\\test_data


![img.png](img.png)